<?php

function lg_enqueue_styles_scripts() {
    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], filemtime(get_template_directory() . '/style.css') );
	
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Poppins:400,500,500i', false );
	wp_enqueue_style( 'vendorCss', get_stylesheet_directory_uri() . '/assets/dist/css/vendor.min.css', filemtime(get_template_directory() . '/assets/dist/css/vendor.min.css') );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/script.min.js") );
	wp_register_script( 'vendorJS', get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/vendor.min.js") );
	wp_register_script( 'alton', get_stylesheet_directory_uri() . "/assets/dist/js/jquery.alton.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/jquery.alton.min.js") );


	wp_enqueue_script( 'lg-script' );
	wp_enqueue_script( 'vendorJS' );
	wp_enqueue_script( 'alton' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function lg_dequeue_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );

?>