<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site pageWrapper">

	<header id="masthead" class="site-header <?php if(is_front_page()): ?>sticky-header<?php endif; ?>">
    <div class="header-content">

      <!--<?php get_template_part("/templates/template-parts/header/site-utility-bar"); ?>-->

      <div class="header-main">
    		<div class="site-branding">
          <div class="logo">
            <?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
          </div>
          <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
    		</div><!-- .site-branding -->

        <div class="main-navigation">
            <div class="menu-logo"><img style="width: 120px;" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/QikMaids-LogoWhite.png'; ?>"></div>
          	<nav class="navbar navbar-default">  
          	    <!-- Brand and toggle get grouped for better mobile display -->
          	    <div class="navbar-header">
          	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
          	        <span class="sr-only">Toggle navigation</span>
          	        <span class="icon-bar"></span>
          	        <span class="icon-bar"></span>
          	        <span class="icon-bar"></span>
          	      </button>
          	    </div>


                <!-- Main Menu  -->
                <?php 

                  $mainMenu = array(
                  	// 'menu'              => 'menu-1',
                  	// 'theme_location'    => 'top-nav',
                  	'depth'             => 2,
                  	'container'         => 'div',
                  	'container_class'   => 'collapse navbar-collapse',
                  	'container_id'      => 'main-navbar',
                  	'menu_class'        => 'nav navbar-nav',
                  	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                  		// 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
                  	'walker'            => new WP_Bootstrap_Navwalker()
                  );
                  wp_nav_menu($mainMenu);

                ?>
          	</nav>
        </div>
      </div>
    </div>

  </header><!-- #masthead -->
