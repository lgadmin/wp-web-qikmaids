function sticky_header_initialize(){

	if(jQuery('.sticky-header')[0]){
		var headerHeight = jQuery('#masthead').outerHeight();
	    jQuery('.slide .overlay-outer-wrap').css('paddingTop', headerHeight);
		jQuery('#masthead').fadeIn();

		if(jQuery(window).width() < 769){
			jQuery('nav.navbar').css('max-height', 'calc( 100vh - '+ headerHeight +'px )');
		}else{
			jQuery('nav.navbar').css('max-height', 'auto');
		}
	}
}
