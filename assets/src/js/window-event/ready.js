// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
          $('.main-navigation').toggleClass('show');
      	});

        $('.dropdown-toggle').on('click', function(e){
          e.preventDefault();
        });

        $('.dropdown-toggle').closest('li').on('mouseover', function(e){
          $(this).addClass('open');
        });

        $('.dropdown-toggle').closest('li').on('mouseleave', function(e){
          $(this).removeClass('open');
        });

      $('.scroll-to-top').on('click', function(){
        var settings = {
                duration: 2 * 1000,
                offset: 0
            };

        KCollection.headerScrollTo($('body'), settings);
      });

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('a').on('click', function(e){
          var href = $(this).attr('href');

          if(href.startsWith('#') && !isNaN(href.substring(1))){
            e.preventDefault();

            if($('.main-navigation').hasClass('show')){
              $('.main-navigation').removeClass('show');
            }

            var scrollTo = $('.leaf-item')[href.substring(1) - 1].closest('.slide');
            var settings = {
                duration: 2 * 1000,
                offset: 0
            };

            KCollection.headerScrollTo(scrollTo, settings);
          }
        });

      }else{

        $('a').on('click', function(e){
          var href = $(this).attr('href');

          if(href.startsWith('#') && !isNaN(href.substring(1))){
            e.preventDefault();

            if($('.main-navigation').hasClass('show')){
              $('.main-navigation').removeClass('show');
            }

            $('#slide-numbers .paginate:nth-child('+href.substring(1)+')').click();
          }
        });

      }
        
    });
}(jQuery));