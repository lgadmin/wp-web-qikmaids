// Windows Load Handler

(function($) {

    $(window).on('load', function(){

    	sticky_header_initialize();


        $('.overlay').each(function(){
    		
          var slide = $(this).closest('.slide');

          if($(this).outerHeight() + $('.site-header').outerHeight() > slide.height()){
          	slide.css('height', $(this).outerHeight() + $('.site-header').outerHeight());
          }
        
        });

        AOS.init();

        // $(document).alton({
        //   fullSlideContainer : 'full', // full page elements container for 
        //   singleSlideClass : 'slide', // class for each individual slide
        //   nextElement : 'div', // set the first element in the first page series.
        //   previousClass : null, // null when starting at the top. Will be updated based on current postion
        //   lastClass: 'site-footer', // last block to scroll to
        //   slideNumbersContainer: 'slide-numbers', // ID of Slide Numbers
        //   bodyContainer: 'pageWrapper', // ID of content container
        //   scrollMode: 'featuredScroll', // Choose scroll mode
        //   useSlideNumbers: true, // Enable or disable slider
        //   slideNumbersBorderColor: '#fff', // outside color for slide numbers
        //   slideNumbersColor: '#000', // interior color when slide numbers inactive
        // });

    });

}(jQuery));