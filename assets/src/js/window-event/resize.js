// Window Resize Handler

(function($) {

    $(window).on('resize', function(){

    	sticky_header_initialize();

       $('.overlay').each(function(){
    		
          var slide = $(this).closest('.slide');

          if($(this).outerHeight() + $('.site-header').outerHeight() > slide.height()){
          	slide.css('height', $(this).outerHeight() + $('.site-header').outerHeight());
          }
        
        });

    })

}(jQuery));