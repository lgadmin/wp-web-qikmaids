<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer bg-primary">
		
		<div id="site-footer" class="text-white clearfix py-3 px-3 container">
			<div class="site-footer-alpha"><?php dynamic_sidebar('footer-alpha'); ?></div>
			<div class="site-footer-bravo"><?php dynamic_sidebar('footer-bravo'); ?></div>
			<div class="site-footer-charlie"><?php dynamic_sidebar('footer-charlie'); ?></div>
		</div>

		<div id="site-legal" class="py-2 px-2 clearfix container">
			<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
		</div>

	</footer><!-- #colophon -->

	<div class="scroll-to-top">
		<i class="fas fa-chevron-up"></i>
	</div>

<?php wp_footer(); ?>

</body>
</html>
