<?php if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();
		
		switch ( get_row_layout()) {
			case 'leaf_section':
				get_template_part('/components/acf-flexible-layout/layouts/leaf_section');
			break;
			case 'form':
				get_template_part('/components/acf-flexible-layout/layouts/form');
			break;
		}

	endwhile;

	else : // no layouts found 

endif; ?>