
<div class="container-fluid">
<form id="service_request_form" class="form-horizontal" action="/simplemoves/service_requests" accept-charset="UTF-8" method="post" novalidate=""><input name="utf8" type="hidden" value="✓"><input type="hidden" name="authenticity_token" value="0XxBJBpQZnvRtNgN6DW4NCPV5PMhtaStiVDPA8IK2Bq475WqinwbZKqVz1I1/lKXj8XcsY+qjfz/+6yuyv981Q==">
<input type="hidden" name="from_action" id="from_action" value="iframe_form">
<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">Tell us about yourself</h3>
</div>
<div class="panel-body">
<div class="form-group has-error has-feedback">
<label class="control-label col-sm-3" for="customer_name">Name</label>
<div class="col-sm-8">
<input type="text" name="customer_name" id="customer_name" class="form-control" placeholder="Company or customer name" data-parsley-trigger="focusout" data-parsley-length="[3,40]" data-parsley-error-message="Please tell us your name." data-parsley-required="true" data-parsley-id="4752">
<span class="form-control-feedback  fa fa-fw fa-times"></span>
<small class="help-block filled" id="parsley-id-4752"><span class="parsley-custom-error-message">Please tell us your name.</span></small></div>
</div>
<div class="form-group">
<label class="control-label col-sm-3" for="customer_email_address">Email</label>
<div class="col-sm-8">
<input type="email" name="customer_email_address" id="customer_email_address" class="form-control" data-parsley-trigger="focusout" data-parsley-error-message="Please enter a valid email address." data-parsley-required="true" data-parsley-id="4231">
<span class="form-control-feedback"></span>
<small class="help-block" id="parsley-id-4231"></small></div>
</div>
<div class="form-group">
<label class="control-label col-sm-3" for="customer_phone_number">Phone</label>
<div class="col-sm-8">
<input type="text" name="customer_phone_number" id="customer_phone_number" class="form-control" data-parsley-id="1035">
<small class="help-block" id="parsley-id-1035"></small></div>
</div>
<div class="form-group">
<label class="control-label col-sm-3" for="customer_service_address">Address</label>
<div class="col-sm-8">
<textarea name="customer_service_address" id="customer_service_address" rows="2" class="form-control" data-parsley-id="6833"></textarea>
<div class="form-inline" style="margin-top: 5px;">
<label class="sr-only">City</label>
<input type="text" name="customer_service_city" id="customer_service_city" class="form-control" placeholder="City" style="width: 40%" data-parsley-id="5483">
<label class="sr-only">Province</label>
<input type="text" name="customer_service_state" id="customer_service_state" class="form-control" placeholder="Province" style="width: 22%" data-parsley-id="4292">
<label class="sr-only">Postal Code</label>
<input type="text" name="customer_service_zip_code" id="customer_service_zip_code" class="form-control" placeholder="Postal Code" style="width: 30%" data-parsley-id="5474">
</div>
<small class="help-block" id="parsley-id-6833"></small><small class="help-block" id="parsley-id-5483"></small><small class="help-block" id="parsley-id-4292"></small><small class="help-block" id="parsley-id-5474"></small></div>
</div>
<div class="form-group">
<label class="control-label col-sm-3" for="customer_source_id">Where did you hear of us?</label>
<div class="col-sm-8">
<select name="customer_source_id" id="customer_source_id" class="form-control" data-parsley-id="5865"><option value="88226">Choose one</option>
<option value="88226">Other</option>
<option value="80740">Offline / Previous Client</option>
<option value="63178">Offline / Team Member or Saw a Vehicle</option>
<option value="84921">Offline / What's On Langley Magazine</option>
<option value="62170">Online / Google</option>
<option value="83170">BNI / Member, Visitor or Guest</option>
<option value="79789">Online / BBB</option>
<option value="76650">Online / Facebook</option>
<option value="62168">Online / Yelp</option>
<option value="62172">Online / Homestars</option></select>
<small class="help-block" id="parsley-id-5865"></small></div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">What can we do for you?</h3>
</div>
<div class="panel-body">
<div class="form-group">
<label class="control-label col-sm-3" for="job_job_type_id">Service</label>
<div class="col-sm-8">
<select name="job_job_type_id" id="job_job_type_id" class="form-control" data-parsley-required="true" data-parsley-id="3224"><option value="">Choose a Service</option><option value="56589">Local Moving</option>
<option value="66245">Storage Moving</option>
<option value="66495">Commercial or Office Moving</option>
<option value="66489">Long Distance Moving</option>
<option value="66491">Moving Labour Help</option>
<option value="56590">Cleaning Services</option></select>
<small class="help-block" id="parsley-id-3224"></small></div>
</div>
<div class="form-group">
<label class="control-label col-sm-3" for="job_description">Description</label>
<div class="col-sm-8">
<textarea name="job_description" id="job_description" class="form-control" rows="2" placeholder="(optional)" data-parsley-id="2394"></textarea>
<small class="help-block" id="parsley-id-2394"></small></div>
</div>
</div>
</div>
<div class="col-sm-6 col-sm-offset-3">
<input type="hidden" name="layout_name" id="layout_name" value="iframe_portal">
<button class="btn btn-success btn-block ladda-button" data-style="zoom-out" type="submit">
<span class="ladda-label">Submit Request</span>

<span class="ladda-spinner"></span></button>
</div>
</form>



</div>

<div class="js-paloma-hook" data-id="1533744856160">
  <script type="text/javascript">
    (function(){
      // Do not continue if Paloma not found.
      if (window['Paloma'] === undefined) {
        return true;
      }

      Paloma.env = 'production';

      // Remove any callback details if any
      $('.js-paloma-hook[data-id!=' + 1533744856160 + ']').remove();

      var request = {"resource":"ServiceRequests","action":"iframe_form","params":{}};

      Paloma.engine.setRequest(
        request['resource'],
        request['action'],
        request['params']);
    })();
  </script>
</div>


<div id="fancybox-tmp"></div><div id="fancybox-loading"><div></div></div><div id="fancybox-overlay"></div><div id="fancybox-wrap"><div id="fancybox-outer"><div class="fancy-bg" id="fancy-bg-n"></div><div class="fancy-bg" id="fancy-bg-ne"></div><div class="fancy-bg" id="fancy-bg-e"></div><div class="fancy-bg" id="fancy-bg-se"></div><div class="fancy-bg" id="fancy-bg-s"></div><div class="fancy-bg" id="fancy-bg-sw"></div><div class="fancy-bg" id="fancy-bg-w"></div><div class="fancy-bg" id="fancy-bg-nw"></div><div id="fancybox-inner"></div><a id="fancybox-close"></a><a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a><a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a></div></div><div id="textarea_simulator" contenteditable="true" style="position: absolute; top: 0px; left: 0px; visibility: hidden;"></div>