<?php 
/**
 * Media Block Layout
 *
 */
?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	$background_type = get_sub_field('background_type');
	$background_colour = get_sub_field('background_colour');
	$background_image = get_sub_field('background_image');
	$background_video = get_sub_field('background_video');

	$overlay = get_sub_field('overlay');
	$overlay_position = $overlay['overlay_position'];
	$overlay_width = $overlay['overlay_width'];
	$overlay_text = $overlay['overlay_text'];
	$overlay_panel_background = $overlay['overlay_panel_background'];

?>

	<?php if($background_type == 'Colour' && $background_colour): ?>
		<div class="slide <?php echo $background_colour; ?>">
	<?php elseif($background_type == 'Image' && $background_image): ?>
		<div class="slide" style="background-image: url('<?php echo $background_image; ?>');">
	<?php else: ?>
		<div class="slide">
	<?php endif; ?>

		<?php if($background_type == 'Video' && $background_video): ?>
			<video autoplay loop muted>
			  <source src="<?php echo $background_video; ?>">
			</video>
		<?php endif; ?>

		<div class="overlay-outer-wrap <?php if($overlay_position){echo $overlay_position;} ?>">
			<div class="overlay-wrap <?php if($overlay_panel_background == 1){echo 'bg';} ?>">
				<div class="overlay" style="<?php if($overlay_width){echo 'max-width:'.$overlay_width;} ?>">
					<div class="overlay-content">
						<?php echo $overlay_text; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf-item"></div>
	</div>
	<div class="section-breaker pb-1 bg-white"></div>

<!--------------------------------------------------------------------------------------------------------------------------------->
